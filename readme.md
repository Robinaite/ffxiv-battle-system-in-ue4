# FFXIV Battle System Recreation

For one of my University Modules Irecreated a majority of the FFXIV Battle System in Unreal Engine 4. This was successfully achieved, with it being possible to recreate, approximately, 75% of all the battle actions in game.

The objective to recreate the battle system of FFXIV was mostly due to my personal interest in the game, as well as high knowledge of how the battle system works. Initially, I needed to research and test how abilities worked, as well as how I could separate them in logical bits that could be easily implemented and augmented. With this project I had the following goals:

- Improve on creating modular C++ code in Unreal Engine 4
- Understand how and what game designers need access to, to allow the creation of the actions
- Make it as modular as possible, to allow for easy extension and adaption to any kind of actions.

With the goals also came some decisions to lower the project scope:

- No visuals or animations – All the actions work and could be easily tied to animations and visuals. Thus, the whole game is shown through text pop-ups.
- Simple input system - Easily replaced and improved upon if time allows, but not necessary for the goals wanted.
- Simple Target System – Easily replaced and improved upon if time allows, but not necessary for the goals wanted.
- No basic attacks
- Concrete implementation of a couple of actions only (6 White Mage Actions specifically)

The following actions are implemented:

- Glare – a single target damage spell, with 2.5 seconds cast timer and 2.5s cooldown.
- Dia – A Damage Over Time spell, which is added to the target for a specific amount of time
- Cure 1 – A single target healing spell
- Cure 2 – An improved single target healing spell
- Medica 1 – An AOE Healing spell
- Presence of Mind – A buff that lowers cast and cooldown timers.

After some research, I developed the following diagram, which contains all the information an action needs to have and that can be changed.

![FFXIV Diagram](ReadmeImages/FFXIV_Diagram.jpg)  
_Figure 1 - FFXIV Battle System Action Diagram_

Here is a preview of the actions implemented (click to open):

![Actions Preview](FFXIV_Action_Preview.m4v)

## Features

The battle system framework allows for the following:

- Creating unlimited Actions
- Creating unlimited Action Types
- Creating unlimited Cast Times
- Creating unlimited Cooldowns, which can be shared between actions
- Create unlimited Resource Types
- Create unlimited Costs based resource types
- Create unlimited Status Effects

### Action

An action is equivalent to one button press and is the data that contains all the information regarding the respective action. In Unreal Engine 4, an Action is a Data Asset, which allows for easier creation, updating and expandability.

![Action Data Asset](ReadmeImages/Action.png)  
_Figure 2 - Action Glare Data Asset_

- Action name – The name of the Action
- Action Type - the type of the action
- Cooldown – The Cooldown of the Action, this cooldown can be shared between multiple actions
- Cost – The cost of using this action
- Cast – The cast time of this action, which during it u are not allowed to move
- Target Priority List – a priority list on which target type one can execute this action upon
- Effect – the effect of successfully executing this action, created by the designers in Blueprints

The actions Effect allow for:

- Updating Resources of Target or Creator
- Adding Status Effects
- Checking if it has a Status Effect
- Obtain multiple targets based on type using amount closest to target
- Obtain multiple targets based on type using distance from target

![Glare Blueprint Example of Action Effect](ReadmeImages/ActionsEffect.png)  
_Figure 3 - Glare Effect Blueprint Implementation Example_

### Action Type

An Action type is to define groups of actions, which can potentially be used for increasing or decreasing specific effects on actions when a status effect is active.

![Action Type Data Asset](ReadmeImages/ActionType.png)  
_Figure 4 - Action Type Data Asset_

- Type Name – The name of the action type

### Cast Time

Each action requires a cast time, this is the time the player needs to stand still (if no special status effects are active) to cast the action that uses the respective cast times data asset. If the time is set to 0, it means instant cast.

![Cast Time Data Asset](ReadmeImages/CastTime.png)  
_Figure 5 - Instant Cast Data Asset_

- Time – The time the cast takes in seconds

### Cooldown Time

Each action requires a cooldown, this is the time which the player needs to wait between using an action. Each action can assign its own cooldown, or share one with other actions, and thus sharing the cooldown.

![Cooldown Time Data Asset](ReadmeImages/CooldownTime.png)  
_Figure 6 - Global Cooldown Data Asset_

- Time – The time the cooldown takes in seconds

### Resources

U can create various resources, for example, Health and Mana. Or it can be used for specific resources for special actions.

![Resource Data Asset](ReadmeImages/Resource.png)  
_Figure 7 - Health Resource Data Asset_

- Resource Name – the name of the resource
- Max Value – the maximum value this resource can get to
- Default value – the starting value of this resource

### Action Cost

Each action can have a Cost associated to it. This cost is a combination of a Resource (shown above) with the value amount of its cost.

![Action Cost Data Asset](ReadmeImages/ActionCost.png)  
_Figure 8 - Mana Cost 1000 Data Asset_

- Amount – The cost amount
- Resource Type – the resource affected

### Status Effect

A status effect is a buff or debuff which a battle target can get for a specific duration.

![Status Effect Data Asset](ReadmeImages/StatusEffect.png)  
_Figure 9 - Dia Status Effect_

- Status name – the name of the status effect
- Duration – how long the status effect exists on the target when added
- Uses Tick Rate – will execute effects each tick (needed for Damage over time, DoT, or Healing over time, HoT)
- Overrides – Only one effect per target is allowed, thus overriding it when re-added.
- Removable by Actions – Allow it to be removed by specific actions
- Effect – The effects of this status effect

The status effect allows for the following:

- A DoT or HoT, with &quot;Uses Tick Rate&quot; activated, same setup as an Action. (Figure 10)
- Changing Cast, Cooldown or Potency values in percentage by overriding the respective function in the Blueprint
- Change updated resource values prior to getting applied in percentage by overriding the respective function in the blueprint

![Status Effect Dia Effect Blueprint](ReadmeImages/DiaStatusEffect.png)  
_Figure 10 - Dia Status Effect Blueprint_

![Status Effect presence of Mind Effect Blueprint](ReadmeImages/PoMStatusEffect.png)  
_Figure 11 - Overridden Obtain Affected Value Amount function for Presence of Mind Status Effect_

Below u can find a video on the process of creating a new action in Unreal Engine 4 (click image to open link):

![Actions Preview](FFXIV_Create_Action_Explanation.m4v)
