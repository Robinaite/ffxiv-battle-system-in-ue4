﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ResourceTypeData.h"
#include "UObject/Object.h"
#include "Effect.generated.h"

class UBattleInfoComponent;

UENUM(BlueprintType)
enum class EAffectedValue : uint8 //TODO Move to a flag so one can select multiple options
{
	None,
	Potency,
	CastTime,
	CooldownTime
};
/**
 * 
 */
UCLASS(Blueprintable)
class FFXIV_BATTLE_SYSTEM_API UEffect : public UObject
{
	GENERATED_BODY()
public:
	
	/**
	 * @brief Execute the effects implemented in Blueprints
	 * @param BattleComponent For Actions this is the Creator, for Status Effect this is the component on which the status effect is on.
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void ExecuteEffects(UBattleInfoComponent* BattleComponent);
	
	/**
	 * @brief Obtains the value of the buff which will affect the cast, cooldown, or Potency Values
	 * @param AffectedValue
	 * @param AmountAffected The amount that should affect the value. For Cast and Cooldown its most often a percentage value between -100 and 100.
	 * Negative meaning it will worsen the affected value, and positive it will improve the affected value;
	 */
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable)
	void ObtainAffectedValue(EAffectedValue AffectedValue,int32& AmountAffected);

	
	/**
	 * @brief Obtains the amount a specific resource type will be affected by
	 * @param ResourceTypeData The resource type that will be affected
	 * @param AmountAffectedInPercentage The amount this resource will be affected, as a percentage value between -100 and 100;
	 * Negative meaning it will worsen the affected value, and positive it will improve the affected value;
	 */
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable)
    void ObtainAffectedResourceValue(UResourceTypeData* ResourceTypeData,int32& AmountAffectedInPercentage);
	
};
