﻿#pragma once

class UBattleInfoComponent;
class UActionData;

DECLARE_DELEGATE(FExecuteActionSignature);

class FAction
{
public:
	FAction(const UActionData* ActionDataBase,UBattleInfoComponent* BattleOwner);

	void StartAction();
private:
	const UActionData* ActionData;
	UBattleInfoComponent* BattleOwner;
	FExecuteActionSignature FAction_ExecuteAction;

	bool CheckCost() const;
	bool CheckTarget() const;
	bool StartCooldown();
	void StartCast();
	UFUNCTION()
	void ExecuteAction();
	bool RemoveCostFromPlayer();
	
};
