// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"

#include "Action.h"
#include "Components/ActorComponent.h"
#include "TargetManager.h"
#include "BattleInfoComponent.generated.h"


class UStatusEffectData;
class FStatusEffect;
class UCastData;
class UCooldownData;
class UCostData;
class UResourceTypeData;
class FAction;
class UActionData;



UENUM()
enum class EInput
{
	Default,
	Slot1,
    Slot2,
    Slot3,
    Slot4,
    Slot5,
    Slot6,
    Slot7,
    Slot8,
    Slot9,
    Slot1Shift,
    Slot2Shift,
    Slot3Shift,
    Slot4Shift,
    Slot5Shift,
    Slot6Shift,
    Slot7Shift,
    Slot8Shift,
    Slot9Shift,
    Slot1Ctrl,
    Slot2Ctrl,
    Slot3Ctrl,
    Slot4Ctrl,
    Slot5Ctrl,
    Slot6Ctrl,
    Slot7Ctrl,
    Slot8Ctrl,
    Slot9Ctrl,
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FFXIV_BATTLE_SYSTEM_API UBattleInfoComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBattleInfoComponent();

	void SetupBattleInput(UInputComponent* InputController);

	bool HasEnoughResources(UCostData* CostData) const;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
                               FActorComponentTickFunction* ThisTickFunction) override;
	void ExecuteAction(const EInput Input);

	bool CreateNewCooldown(UCooldownData* CooldownData);
	bool CreateNewCast(UCastData* CastData,FExecuteActionSignature& ActionToExecute);

	UFUNCTION()
	void TryInterruptCast();

	UFUNCTION(BlueprintCallable,Category="Battle|Targets")
	bool IsTargetType(const ETargetType Target) const;

	bool IsSelectedTargetOfType(const ETargetType Type) const;

	UFUNCTION(BlueprintCallable,Category="Battle|Targets")
	UTargetManager* ObtainTargetManager() const;

	void UpdateResources(UResourceTypeData* ResourceType,const int32 Amount);

	void AddStatusEffect(UStatusEffectData* StatusEffectData, UBattleInfoComponent* StatusCreator);
	void RemoveStatusEffect(FStatusEffect* StatusEffect);
	void RemoveStatusEffect(UStatusEffectData* StatusEffectData);
	bool HasStatusEffect(UStatusEffectData* StatusEffectData) const;

	int32 ObtainAffectedStatusEffectValues(UResourceTypeData* ResourceType,const int32 OriginalAmount);

	//Delegates for UI mostly
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FResourceUpdated,const UResourceTypeData*,ResourceType,const int32,TotalAmount);
	DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FAlertText,const FString,Text,const FColor,TextColor);

	UPROPERTY(BlueprintAssignable)
	FResourceUpdated ResourceUpdated;

	UPROPERTY(BlueprintAssignable)
	FAlertText LogToScreen;
	
protected:
	UPROPERTY(EditAnywhere)
	TMap<EInput,UActionData*> ActionsData;
	
	UPROPERTY(EditAnywhere)
	TArray<UResourceTypeData*> Resources;
	
	
	UPROPERTY(EditAnywhere)
	ETargetType TargetType{ETargetType::Enemy};

	UPROPERTY(EditDefaultsOnly,Category="Components")
	UTargetManager* TargetManager;
	
	// Called when the game starts
	virtual void BeginPlay() override;
	
	virtual void BeginDestroy() override;

private:
	TMap<EInput,FAction*> ActionsInstantiated;
	TMap<UResourceTypeData*,int32> ResourcesInstantiated;
	TMap<UCooldownData*,FTimerHandle> CooldownDataTimer;
	TArray<FStatusEffect*> StatusEffects;
	
	FTimerHandle CastTimer;
	FTimerManager* WorldTimerManager;
	
	/**
	 * @brief This is set to the last started cooldown. It is used to interrupt it, in case a cast is interrupted
	 */
	FTimerHandle* LastStartedCooldown;
};
