// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ResourceTypeData.generated.h"

/**
 * 
 */
UCLASS()
class FFXIV_BATTLE_SYSTEM_API UResourceTypeData : public UDataAsset
{
	GENERATED_BODY()

public:
	int32 GetMaxValue() const;
	int32 GetDefaultValue() const;
protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Resource")
	FName ResourceName;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Resource")
	int32 MaxValue;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Resource")
	int32 DefaultValue;



	
};
