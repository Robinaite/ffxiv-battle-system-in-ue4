﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

enum class EAffectedValue : uint8;
class UResourceTypeData;
class UBattleInfoComponent;
class UStatusEffectData;

constexpr float TickRate = 1.0f; //Every 0.2 of a second it will execute the effect

/**
 * 
 */
class FFXIV_BATTLE_SYSTEM_API FStatusEffect
{
public:
	FStatusEffect(const UStatusEffectData* AStatusEffectData,UBattleInfoComponent* TheBattleTarget,UBattleInfoComponent* TheBattleCreator);
	~FStatusEffect();

	void ApplyEffect();
	void DurationFinished();

	bool IsStatusEffect(const UStatusEffectData* AStatusEffectData,const UBattleInfoComponent* TheBattleCreator) const;
	bool IsStatusEffect(const UStatusEffectData* AStatusEffectData) const;
	bool IsRemovable() const;

	int32 ObtainEffectResourceTypeValue(UResourceTypeData* AStatusEffectData) const;
	int32 ObtainAffectedValue(EAffectedValue& AffectedValue) const;
	

private:
	const UStatusEffectData* StatusEffectData;
	UBattleInfoComponent* BattleTarget;
	UBattleInfoComponent* BattleCreator;
	FTimerHandle DurationTimerHandler;
	FTimerHandle TickTimerHandle;
};
