// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"



#include "TargetManager.h"
#include "Engine/DataAsset.h"
#include "ActionData.generated.h"

class UEffect;
class UCooldownData;
class UCastData;
class UCostData;
class UActionTypeData;
/**
 * 
 */
UCLASS()
class FFXIV_BATTLE_SYSTEM_API UActionData : public UDataAsset
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Action")
	FName ActionName;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Action")
	UActionTypeData* ActionType;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Action")
	UCooldownData* Cooldown;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Action")
	UCostData* Cost;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Action")
	UCastData* Cast;

	/**
	 * @brief Priority List of the Targets. Order this list to indicate what target is priority to cast the action on.
	 */
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Action")
	TArray<ETargetType> TargetPriorityList;
	
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Action")
	TSubclassOf<UEffect> Effect;


public:
	FName GetActionName() const;
	UCastData* GetCastData() const;
	UCostData* GetCostData() const;
	UCooldownData* GetCooldownData() const;
	TArray<ETargetType> GetTargetPriorityList() const;
	TSubclassOf<UEffect> GetEffects() const;
};
