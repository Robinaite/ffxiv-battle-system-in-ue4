// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "CastData.generated.h"

/**
 * 
 */
UCLASS()
class FFXIV_BATTLE_SYSTEM_API UCastData : public UDataAsset
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Cast",Meta=(ClampMin="0"))
	float Time;

public:
	bool HasCastTime() const;
	float GetCastTime() const;
};
