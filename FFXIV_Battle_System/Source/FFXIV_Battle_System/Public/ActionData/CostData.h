// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "CostData.generated.h"

class UResourceTypeData;
/**
 * 
 */
UCLASS()
class UCostData : public UDataAsset
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Cost",Meta=(ClampMin="0"))
	int32 Amount;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Cost")
	UResourceTypeData* ResourceType;

public:
	UResourceTypeData* GetResourceType() const;
	int32 GetCostAmount() const;
	
};
