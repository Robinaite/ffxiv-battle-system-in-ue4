// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "CooldownData.generated.h"

/**
 * 
 */
UCLASS()
class FFXIV_BATTLE_SYSTEM_API UCooldownData : public UDataAsset
{
	GENERATED_BODY()
public:
	float GetTime() const;
protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Cooldown",Meta=(ClampMin="0"))
	float Time;


};
