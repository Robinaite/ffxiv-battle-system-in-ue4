// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ActionTypeData.generated.h"

/**
 * 
 */
UCLASS()
class FFXIV_BATTLE_SYSTEM_API UActionTypeData : public UDataAsset
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	FName TypeName;
};
