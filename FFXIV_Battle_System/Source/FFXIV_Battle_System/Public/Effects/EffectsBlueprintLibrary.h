// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "BattleInfoComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EffectsBlueprintLibrary.generated.h"

/**
 * 
 */
UCLASS()
class FFXIV_BATTLE_SYSTEM_API UEffectsBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable,Category="Battle|Effects")
	static void UpdateResource(UBattleInfoComponent* Target,UResourceTypeData* Resource,const int32 Amount);

	UFUNCTION(BlueprintCallable,Category="Battle|Effects")
	static void AddStatusEffect(UBattleInfoComponent* Target,UStatusEffectData* StatusEffect,UBattleInfoComponent* Creator);

	UFUNCTION(BlueprintCallable,Category="Battle|Effects")
	static bool HasStatusEffect(UBattleInfoComponent* Target,UStatusEffectData* StatusEffect);

	UFUNCTION(BlueprintCallable,Category="Battle|Effects")
	static int32 ObtainResourceAmountAffectedFromCreator(UBattleInfoComponent* Creator,UResourceTypeData* Resource,const int32 Amount);

	UFUNCTION(BlueprintCallable,Category="Battle|Effects")
	static void RemoveStatusEffect(UBattleInfoComponent* Target,UStatusEffectData* StatusEffect);
	
};
