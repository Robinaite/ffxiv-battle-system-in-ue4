// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "StatusEffectData.generated.h"

class UEffect;



/**
 * 
 */
UCLASS()
class FFXIV_BATTLE_SYSTEM_API UStatusEffectData : public UDataAsset
{
	GENERATED_BODY()
protected:
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Status")
	FName StatusName;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Status",Meta=(ClampMin="0"))
	float Duration;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Status",Meta=(ClampMin="0"))
	bool UsesTickRate = false;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Status")
	bool Overrides = false;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Status")
	bool RemovableByActions = false;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Status")
	TSubclassOf<UEffect> Effect;
	
public:
	float GetDuration() const;
	bool IsUsingTickRate() const;
	bool OverridesSameStatus() const;
	bool IsRemovable() const;
	TSubclassOf<UEffect> GetEffect() const;
	
};
