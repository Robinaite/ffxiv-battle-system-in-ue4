﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/ActorComponent.h"
#include "TargetManager.generated.h"

UENUM(BlueprintType)
enum class ETargetType : uint8
{
	Enemy,
	Ally,
	Self
};

class UBattleInfoComponent;
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class FFXIV_BATTLE_SYSTEM_API UTargetManager : public UActorComponent
{
	GENERATED_BODY()


public:
	static void RegisterTarget(UBattleInfoComponent* BattleInfoComponent);
	static void UnregisterTarget(UBattleInfoComponent* BattleInfoComponent);
private:
	 static TArray<UBattleInfoComponent*> PossibleTargetsInWorld;

public:
	// Sets default values for this component's properties
	UTargetManager();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
                               FActorComponentTickFunction* ThisTickFunction) override;
	
	/**
	 * @brief Return the selected target if any.
	 * @return Return the selected target if any, or nullptr otherwise.
	 */
	UFUNCTION(BlueprintCallable,Category="Battle|Targets")
	UBattleInfoComponent* GetSelectedTarget() const;

	void SetSelectedTargetClosestToInView();

	UFUNCTION(BlueprintCallable,Category="Battle|Targets")
	static TArray<UBattleInfoComponent*> ObtainClosestTargetsFromTarget(const UBattleInfoComponent* Target,const int AmountOfTargets,const ETargetType TargetType);
	UFUNCTION(BlueprintCallable,Category="Battle|Targets")
	static TArray<UBattleInfoComponent*> ObtainTargetsInRange(const UBattleInfoComponent* Target,const int Range,const ETargetType TargetType);
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UBattleInfoComponent* SelectedTarget;
};
