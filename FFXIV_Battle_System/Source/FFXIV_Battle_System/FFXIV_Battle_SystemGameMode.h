// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FFXIV_Battle_SystemGameMode.generated.h"

UCLASS(minimalapi)
class AFFXIV_Battle_SystemGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFFXIV_Battle_SystemGameMode();
};



