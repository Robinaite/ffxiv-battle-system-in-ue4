// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class FFXIV_Battle_System : ModuleRules
{
	public FFXIV_Battle_System(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
