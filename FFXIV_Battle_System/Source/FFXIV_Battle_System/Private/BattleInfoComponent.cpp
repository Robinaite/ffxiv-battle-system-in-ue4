// Fill out your copyright notice in the Description page of Project Settings.


#include "BattleInfoComponent.h"
#include "Action.h"
#include "Effect.h"
#include "ResourceTypeData.h"
#include "StatusEffect.h"
#include "TargetManager.h"
#include "ActionData/CastData.h"
#include "ActionData/CooldownData.h"
#include "ActionData/CostData.h"
#include "FFXIV_Battle_System/FFXIV_Battle_SystemCharacter.h"
#include "Components/InputComponent.h"
#include "StatusEffectData/StatusEffectData.h"
#include "StatusEffect.h"

// Sets default values for this component's properties
UBattleInfoComponent::UBattleInfoComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UBattleInfoComponent::BeginPlay()
{
	Super::BeginPlay();

	if(!TargetManager)
	{
		TargetManager = GetOwner()->FindComponentByClass<UTargetManager>();
	}
	
	UTargetManager::RegisterTarget(this);

	//Setup Actions Instances
	WorldTimerManager = &GetOwner()->GetWorldTimerManager();
	for (const auto ActionDataInput : ActionsData)
	{
		FAction* NewAction = new FAction{ActionDataInput.Value,this};
		ActionsInstantiated.Add(ActionDataInput.Key,NewAction);
	}

	//SetupResourceInstances;
	for (auto* ResourceType : Resources)
	{
		ResourcesInstantiated.Add(ResourceType,ResourceType->GetDefaultValue());
	}

	if(TargetType == ETargetType::Self) //TODO Careful, this part is player only. Should be modified if AI can get actions as well.
	{
		AFFXIV_Battle_SystemCharacter* Character = static_cast<AFFXIV_Battle_SystemCharacter*>(GetOwner());
		Character->OnStartMovement.AddUObject(this,&UBattleInfoComponent::TryInterruptCast);
	}
}

void UBattleInfoComponent::SetupBattleInput(UInputComponent* InputController) //TODO Careful, this is a player only method, but this script is used by oth the player and the AI
{
	if(!InputController)
	{
		return;
	}

	DECLARE_DELEGATE_OneParam(FCustomInputDelegate, const EInput);
    InputController->BindAction<FCustomInputDelegate>("Slot1",IE_Pressed,this,&UBattleInfoComponent::ExecuteAction,EInput::Slot1);
	InputController->BindAction<FCustomInputDelegate>("Slot2",IE_Pressed,this,&UBattleInfoComponent::ExecuteAction,EInput::Slot2);
	InputController->BindAction<FCustomInputDelegate>("Slot3",IE_Pressed,this,&UBattleInfoComponent::ExecuteAction,EInput::Slot3);
	InputController->BindAction<FCustomInputDelegate>("Slot4",IE_Pressed,this,&UBattleInfoComponent::ExecuteAction,EInput::Slot4);
	InputController->BindAction<FCustomInputDelegate>("Slot5",IE_Pressed,this,&UBattleInfoComponent::ExecuteAction,EInput::Slot5);
	InputController->BindAction<FCustomInputDelegate>("Slot6",IE_Pressed,this,&UBattleInfoComponent::ExecuteAction,EInput::Slot6);
}

bool UBattleInfoComponent::HasEnoughResources(UCostData* CostData) const
{
	if(const int* CostAmount = ResourcesInstantiated.Find(CostData->GetResourceType())  )
	{
		if(*CostAmount >= CostData->GetCostAmount())
		{
			return true;
		}
	}
	return false;
}





void UBattleInfoComponent::BeginDestroy()
{
	Super::BeginDestroy();

	UTargetManager::UnregisterTarget(this);
	
	for (auto ActionInstantiated : ActionsInstantiated)
	{
		delete ActionInstantiated.Value;
	}
	ActionsInstantiated.Empty();

	ResourcesInstantiated.Empty();
}

// Called every frame
void UBattleInfoComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UBattleInfoComponent::ExecuteAction(const EInput Input)
{
	if(ActionsInstantiated.Num() == 0)
	{
		return;
	}

	if(TargetManager)
	{
		UBattleInfoComponent* comp = TargetManager->GetSelectedTarget();
		if(!comp)
		{
			return;
		}
		UE_LOG(LogTemp,Warning,TEXT("Component obtained. %s"),*comp->GetOwner()->GetName())
	}

	if(FAction** Action = ActionsInstantiated.Find(Input))
	{
		(*Action)->StartAction();
	}
}

bool UBattleInfoComponent::CreateNewCooldown(UCooldownData* CooldownData)
{
	FTimerHandle* HandlePointer = CooldownDataTimer.Find(CooldownData);
	
	if(HandlePointer && WorldTimerManager->IsTimerActive(*HandlePointer)) //Cooldown exists should not execute action
	{
		return false;
	}

	if(!HandlePointer)
	{
		//create the new cooldown
		HandlePointer = &CooldownDataTimer.Emplace(CooldownData);
	}
	LastStartedCooldown = HandlePointer;

	const float InRate = CooldownData->GetTime();
	float AddedInRate = 0;
	EAffectedValue AffectedValue = EAffectedValue::CooldownTime;
	for (auto StatusEffect : StatusEffects)
	{
		const int32 ValueToMultiplyBy = StatusEffect->ObtainAffectedValue(AffectedValue);
		AddedInRate += static_cast<float>(ValueToMultiplyBy)/100 * InRate;
	}

	//Prevents issues if Designer puts wrong values, or the added value goes below 0;
	if(AddedInRate < -InRate)
	{
		AddedInRate = -InRate;
	}
	
	WorldTimerManager->SetTimer(*HandlePointer,InRate+AddedInRate,false);
	return true;
}

bool UBattleInfoComponent::CreateNewCast(UCastData* CastData,FExecuteActionSignature& ActionToExecute)
{
	if(WorldTimerManager->IsTimerActive(CastTimer)) //already a cast going on should drop trying to execute the cast
	{
		return false;
	}

	const float InRate = CastData->GetCastTime();
	float AddedInRate = 0;
	EAffectedValue AffectedValue = EAffectedValue::CastTime;
	for (auto StatusEffect : StatusEffects)
	{
		const int32 ValueToMultiplyBy = StatusEffect->ObtainAffectedValue(AffectedValue);
		AddedInRate += static_cast<float>(ValueToMultiplyBy)/100 * InRate;
	}
	
	//Prevents issues if Designer puts wrong values, or the added value goes below 0;
	if(AddedInRate < -InRate)
	{
		AddedInRate = -InRate;
	}

	if(AddedInRate != 0)
	{
		LogToScreen.Broadcast(AddedInRate < 0 ? TEXT("Cast time Is lower than normal") : TEXT("Cast time is higher than normal") ,FColor::Black);
	}
	
	
	WorldTimerManager->SetTimer(CastTimer,ActionToExecute,InRate + AddedInRate,false);
	return true;
}

void UBattleInfoComponent::TryInterruptCast()
{
	if(WorldTimerManager->IsTimerActive(CastTimer))
	{
		WorldTimerManager->ClearTimer(CastTimer);
		WorldTimerManager->ClearTimer(*LastStartedCooldown);
		LogToScreen.Broadcast(TEXT("Interrupted"),FColor::Black);
	}
}

bool UBattleInfoComponent::IsSelectedTargetOfType(const ETargetType Type) const
{
	return TargetManager->GetSelectedTarget()->IsTargetType(Type);
}

UTargetManager* UBattleInfoComponent::ObtainTargetManager() const
{
	return TargetManager;
}

void UBattleInfoComponent::UpdateResources(UResourceTypeData* ResourceType, const int32 Amount)
{
	if(ResourcesInstantiated.Contains(ResourceType))
	{
		int32 NewValue = ResourcesInstantiated[ResourceType];
		int32 AmountAffectedByStatusEffects = Amount;
		for (auto StatusEffect : StatusEffects)
		{
			const int32 ValueToMultiplyBy = StatusEffect->ObtainEffectResourceTypeValue(ResourceType);
			AmountAffectedByStatusEffects += static_cast<float>(ValueToMultiplyBy)/100 * Amount;
		}
		
		NewValue += AmountAffectedByStatusEffects; 
		
		if(NewValue < 0)
		{
			NewValue = 0;
		} else if(NewValue > ResourceType->GetMaxValue())
		{
			NewValue = ResourceType->GetMaxValue();
		}
		ResourcesInstantiated[ResourceType] = NewValue;
		ResourceUpdated.Broadcast(ResourceType,NewValue);
	}
}

void UBattleInfoComponent::AddStatusEffect(UStatusEffectData* StatusEffectData, UBattleInfoComponent* StatusCreator)
{
	if(StatusEffectData->OverridesSameStatus())
	{
		const auto EffectToRemove = StatusEffects.FindByPredicate(
			[StatusEffectData,StatusCreator](FStatusEffect* Effect)
			{
				return Effect->IsStatusEffect(StatusEffectData,StatusCreator);
			});
	
		if(EffectToRemove)
		{
			StatusEffects.RemoveSingle(*EffectToRemove);
			delete *EffectToRemove;
		}
	}
	
	FStatusEffect* Effect = new FStatusEffect{StatusEffectData,this,StatusCreator};
	StatusEffects.Add(Effect);
}

void UBattleInfoComponent::RemoveStatusEffect(FStatusEffect* StatusEffect)
{
	int32 Index;
	if(StatusEffects.Find(StatusEffect,Index))
	{
		FStatusEffect* Effect = StatusEffects[Index];
		StatusEffects.RemoveSingle(Effect);
		delete Effect;
	}
}

void UBattleInfoComponent::RemoveStatusEffect(UStatusEffectData* StatusEffectData)
{
	const auto EffectToRemove = StatusEffects.FindByPredicate(
            [](FStatusEffect* Effect)
            {
                return Effect->IsRemovable();
            });

	if(EffectToRemove)
	{
		StatusEffects.RemoveSingle(*EffectToRemove);
		delete *EffectToRemove;
	}
}

bool UBattleInfoComponent::HasStatusEffect(UStatusEffectData* StatusEffectData) const
{

	return StatusEffects.ContainsByPredicate([StatusEffectData](FStatusEffect* Effect)
            {
                return Effect->IsStatusEffect(StatusEffectData);
            });
}

int32 UBattleInfoComponent::ObtainAffectedStatusEffectValues(UResourceTypeData* ResourceType,
	const int32 OriginalAmount)
{
	int32 AmountAffectedByStatusEffects = OriginalAmount;
	for (auto StatusEffect : StatusEffects)
	{
		const int32 ValueToMultiplyBy = StatusEffect->ObtainEffectResourceTypeValue(ResourceType);
		AmountAffectedByStatusEffects += static_cast<float>(ValueToMultiplyBy)/100 * OriginalAmount;
	}

	return AmountAffectedByStatusEffects;
}

bool UBattleInfoComponent::IsTargetType(const ETargetType Target) const
{
	return Target == TargetType;
}
