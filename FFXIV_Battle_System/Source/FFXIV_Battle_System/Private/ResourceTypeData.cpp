// Fill out your copyright notice in the Description page of Project Settings.


#include "ResourceTypeData.h"

int32 UResourceTypeData::GetMaxValue() const
{
	return MaxValue;
}

int32 UResourceTypeData::GetDefaultValue() const
{
	return DefaultValue;
}
