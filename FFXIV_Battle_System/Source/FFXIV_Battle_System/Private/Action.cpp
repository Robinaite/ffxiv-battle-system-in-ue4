﻿#include "Action.h"


#include "BattleInfoComponent.h"
#include "Effect.h"
#include "ActionData/ActionData.h"
#include "ActionData/CastData.h"
#include "ActionData/CostData.h"

FAction::FAction(const UActionData* ActionDataBase,UBattleInfoComponent* BattleOwner): ActionData(ActionDataBase),
                                                                                       BattleOwner(BattleOwner)
{
	FAction_ExecuteAction.BindRaw(this,&FAction::ExecuteAction);
}

void FAction::StartAction()
{
	UE_LOG(LogTemp,Warning,TEXT("Action 1 has started!"));
	
	if(!ActionData && !BattleOwner) //Also double check if the data or owner is not null by chance
	{
		return;
	}
	if(!CheckCost()) //Check if the player can execute the action even.
	{
		BattleOwner->LogToScreen.Broadcast(FString::Printf(TEXT("PLayer does not have enough resources for Action %s"),*ActionData->GetActionName().ToString()),FColor::Red);
		return;
	}
	
	UE_LOG(LogTemp,Display,TEXT("Action 1 has enough Mana!"));
	if(!CheckTarget() )
	{
		UE_LOG(LogTemp,Warning,TEXT("Incorrect Target selected"))
		BattleOwner->LogToScreen.Broadcast(FString::Printf(TEXT("Action %s has incorrect target selected"),*ActionData->GetActionName().ToString()),FColor::Red);
		return;
	}
	if(!StartCooldown())
	{
		UE_LOG(LogTemp,Warning,TEXT("Cooldown for this action is ongoing"))
		BattleOwner->LogToScreen.Broadcast(FString::Printf(TEXT("Action %s is on Cooldown"),*ActionData->GetActionName().ToString()),FColor::Red);
		return;
	}
	//Start Cast
	StartCast();
}

bool FAction::CheckCost() const
{
	//Check Cost against player information
	//TODO Missing Cure 1 to Cure 2 no cost heal setup
	return BattleOwner->HasEnoughResources(ActionData->GetCostData());;
}

bool FAction::CheckTarget() const
{
	for (auto TargetType : ActionData->GetTargetPriorityList())
	{
		if(TargetType == ETargetType::Self)
		{
			return true;
		} 
		
		if(BattleOwner->IsSelectedTargetOfType(TargetType))
		{
			return true;
		}
	}
	return false; 
}

bool FAction::StartCooldown()
{
	//Start Cooldown Timer on the player character.
	if(BattleOwner->CreateNewCooldown(ActionData->GetCooldownData()))
	{
		return true;
	}
	return false;
}

void FAction::StartCast()
{
	//Check if the action has a Cast.
	if(ActionData->GetCastData()->HasCastTime())
	{
		if(!BattleOwner->CreateNewCast(ActionData->GetCastData(),FAction_ExecuteAction))
		{
			UE_LOG(LogTemp,Warning,TEXT("Already casting another action."))
			BattleOwner->LogToScreen.Broadcast(FString::Printf(TEXT("Already casting an Action %s"),*ActionData->GetActionName().ToString()),FColor::Red);
			return;
		} else
		{
			BattleOwner->LogToScreen.Broadcast(FString::Printf(TEXT("Start casting Action %s"),*ActionData->GetActionName().ToString()),FColor::Green);
		}
	} else
	{
		ExecuteAction();
	}
}

void FAction::ExecuteAction()
{
	//Remove Costs from character
	if(!RemoveCostFromPlayer())
	{
		UE_LOG(LogTemp,Warning,TEXT("Not enough resources when action was just starting to execute"))
		BattleOwner->LogToScreen.Broadcast(FString::Printf(TEXT("PLayer does not have enough resources for Action %s"),*ActionData->GetActionName().ToString()),FColor::Red);
		return;
	}

	//Execute Effects
	UE_LOG(LogTemp,Display,TEXT("Action 1 is executed."));
	if(UEffect* Effect = ActionData->GetEffects().GetDefaultObject())
	{
		BattleOwner->LogToScreen.Broadcast(FString::Printf(TEXT("Action %s is executed"),*ActionData->GetActionName().ToString()),FColor::Green);
		Effect->ExecuteEffects(BattleOwner);
	}
}

bool FAction::RemoveCostFromPlayer()
{
	if(!CheckCost())
	{
		return false;
	}

	BattleOwner->UpdateResources(ActionData->GetCostData()->GetResourceType(),-ActionData->GetCostData()->GetCostAmount());
	return true;
}
