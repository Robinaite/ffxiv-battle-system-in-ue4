﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TargetManager.h"
#include "BattleInfoComponent.h"

TArray<UBattleInfoComponent*> UTargetManager::PossibleTargetsInWorld{};

void UTargetManager::RegisterTarget(UBattleInfoComponent* BattleInfoComponent)
{
	if(BattleInfoComponent)
	{
		UTargetManager::PossibleTargetsInWorld.AddUnique(BattleInfoComponent);
	}
}

void UTargetManager::UnregisterTarget(UBattleInfoComponent* BattleInfoComponent)
{
	if(BattleInfoComponent)
	{
		UTargetManager::PossibleTargetsInWorld.Remove(BattleInfoComponent);
	}
}

// Sets default values for this component's properties
UTargetManager::UTargetManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTargetManager::BeginPlay()
{
	Super::BeginPlay();

	// ...
	FTimerHandle timer;
	GetWorld()->GetTimerManager().SetTimer(timer,this,&UTargetManager::SetSelectedTargetClosestToInView,1.f); //TODO Change this with better target manager if I ever want to.
}


// Called every frame
void UTargetManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UBattleInfoComponent* UTargetManager::GetSelectedTarget() const
{
	return SelectedTarget;
}

void UTargetManager::SetSelectedTargetClosestToInView()
{

	
	//Gets all battleInfoComponents in the world
	//Check if it is an enemy
	//Check if in view
	//Get closest
	float Distance = TNumericLimits<float>::Max();
	for (auto *BattleInfoComponent : PossibleTargetsInWorld)
	{
		if(BattleInfoComponent->IsTargetType(ETargetType::Enemy))
		{
			const float DistanceBetweenActors = (GetOwner()->GetActorLocation() - BattleInfoComponent->GetOwner()->GetActorLocation()).Size();
			if(Distance > DistanceBetweenActors)
			{
				Distance = DistanceBetweenActors;
				SelectedTarget = BattleInfoComponent;
			}
		}
	}
	UE_LOG(LogTemp,Warning,TEXT("Hey smallest distance is: %f with actor %s"),Distance,*SelectedTarget->GetOwner()->GetName());
	
	//Get the closest one to this
	//Set the selected target to the one obtained.
	
}

TArray<UBattleInfoComponent*> UTargetManager::ObtainClosestTargetsFromTarget(const UBattleInfoComponent* Target,
                                                                             const int AmountOfTargets,const ETargetType TargetType)
{
	TArray<UBattleInfoComponent*> TargetsOrderedByDistance;
	for (auto* TargetInWorld : PossibleTargetsInWorld)
	{
		if(TargetInWorld->IsTargetType(TargetType))
		{
			TargetsOrderedByDistance.Add(TargetInWorld);
		}
	}
	
	TargetsOrderedByDistance.Sort([Target](const UBattleInfoComponent& A,const UBattleInfoComponent& B)
	{
		const FVector TargetLocation = Target->GetOwner()->GetActorLocation();
		const FVector ALoc = A.GetOwner()->GetActorLocation();
		const FVector BLoc = B.GetOwner()->GetActorLocation();
		return FVector::DistSquared(TargetLocation,ALoc) < FVector::DistSquared(TargetLocation,BLoc);
	});

	if(TargetsOrderedByDistance.Num() > AmountOfTargets)
	{
		TargetsOrderedByDistance.SetNum(AmountOfTargets);
	}

	return TargetsOrderedByDistance;
}

TArray<UBattleInfoComponent*> UTargetManager::ObtainTargetsInRange(const UBattleInfoComponent* Target, const int Range,
	const ETargetType TargetType)
{
	TArray<UBattleInfoComponent*> TargetsOrderedByDistance;
	for (auto* TargetInWorld : PossibleTargetsInWorld)
	{
		if(TargetInWorld->IsTargetType(TargetType))
		{
			const FVector TargetLocation = Target->GetOwner()->GetActorLocation();
			const FVector TargetInWorldLocation = TargetInWorld->GetOwner()->GetActorLocation();
			if(FVector::DistSquared(TargetLocation,TargetInWorldLocation) < Range)
			{
				TargetsOrderedByDistance.Add(TargetInWorld);
			}
		}
	}

	return TargetsOrderedByDistance;
}

