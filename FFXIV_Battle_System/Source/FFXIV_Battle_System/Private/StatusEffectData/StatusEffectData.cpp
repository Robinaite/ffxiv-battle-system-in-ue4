// Fill out your copyright notice in the Description page of Project Settings.


#include "StatusEffectData/StatusEffectData.h"

float UStatusEffectData::GetDuration() const
{
	return Duration;
}

bool UStatusEffectData::IsUsingTickRate() const
{
	return UsesTickRate;
}

bool UStatusEffectData::OverridesSameStatus() const
{
	return Overrides;
}

bool UStatusEffectData::IsRemovable() const
{
	return RemovableByActions;
}

TSubclassOf<UEffect> UStatusEffectData::GetEffect() const
{
	return Effect;
}
