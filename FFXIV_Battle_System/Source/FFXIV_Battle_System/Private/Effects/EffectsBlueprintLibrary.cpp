// Fill out your copyright notice in the Description page of Project Settings.


#include "Effects/EffectsBlueprintLibrary.h"

#include "StatusEffectData/StatusEffectData.h"

void UEffectsBlueprintLibrary::UpdateResource(UBattleInfoComponent* Target, UResourceTypeData* Resource,const int32 Amount)
{
	if(!Target || !Resource)
	{
		UE_LOG(LogTemp,Error,TEXT("TArget is not set correctly o Update Resource"));
		return;
	}

	Target->UpdateResources(Resource,Amount);
}

void UEffectsBlueprintLibrary::AddStatusEffect(UBattleInfoComponent* Target, UStatusEffectData* StatusEffect,UBattleInfoComponent* Creator)
{
	if(!Target || !Creator || !StatusEffect)
	{
		UE_LOG(LogTemp,Error,TEXT("TArget, creator or StatusEffect is not assigned correctly o Add Status Effect"));
		return;
	}

	Target->AddStatusEffect(StatusEffect,Creator);
}

bool UEffectsBlueprintLibrary::HasStatusEffect(UBattleInfoComponent* Target, UStatusEffectData* StatusEffect)
{
	if(!Target || !StatusEffect)
	{
		UE_LOG(LogTemp,Error,TEXT("TArget, or StatusEffect is not assigned correctly on Has Status Effect"));
		return false;
	}

	return Target->HasStatusEffect(StatusEffect);
}

int32 UEffectsBlueprintLibrary::ObtainResourceAmountAffectedFromCreator(UBattleInfoComponent* Creator,
	UResourceTypeData* Resource, const int32 Amount)
{
	if(!Creator || !Resource)
	{
		UE_LOG(LogTemp,Error,TEXT("Creator or Resource not set correctly in Obtain Resource Amount Affected From Creator"));
		return Amount;
	}

	return Creator->ObtainAffectedStatusEffectValues(Resource,Amount);
}

void UEffectsBlueprintLibrary::RemoveStatusEffect(UBattleInfoComponent* Target, UStatusEffectData* StatusEffect)
{
	if(!Target || !StatusEffect)
	{
		UE_LOG(LogTemp,Error,TEXT("TArget, or StatusEffect is not assigned correctly on Remove Status Effect"));
		return;
	}

	if(StatusEffect->IsRemovable())
	{
		Target->RemoveStatusEffect(StatusEffect);
	}
}


