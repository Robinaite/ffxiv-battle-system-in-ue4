﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "StatusEffect.h"


#include "BattleInfoComponent.h"
#include "Effect.h"
#include "StatusEffectData/StatusEffectData.h"

FStatusEffect::FStatusEffect(const UStatusEffectData* AStatusEffectData,UBattleInfoComponent* TheBattleTarget,UBattleInfoComponent* TheBattleCreator) :
StatusEffectData(AStatusEffectData),
BattleTarget(TheBattleTarget),
BattleCreator(TheBattleCreator)
{
	BattleTarget->GetWorld()->GetTimerManager().SetTimer(DurationTimerHandler,FTimerDelegate::CreateRaw(this,&FStatusEffect::DurationFinished),StatusEffectData->GetDuration(),false);

	if(StatusEffectData->IsUsingTickRate())
	{
		BattleTarget->GetWorld()->GetTimerManager().SetTimer(TickTimerHandle,FTimerDelegate::CreateRaw(this,&FStatusEffect::ApplyEffect),TickRate,true);
	}
}

FStatusEffect::~FStatusEffect()
{
	BattleTarget->GetWorld()->GetTimerManager().ClearTimer(TickTimerHandle);
	BattleTarget->GetWorld()->GetTimerManager().ClearTimer(DurationTimerHandler);
}

void FStatusEffect::ApplyEffect()
{
	if(!StatusEffectData)
	{
		return;
	}
	
	if( UEffect* Effect = StatusEffectData->GetEffect().GetDefaultObject())
	{
		Effect->ExecuteEffects(BattleTarget);
	}
}

void FStatusEffect::DurationFinished()
{
	BattleTarget->GetWorld()->GetTimerManager().ClearTimer(TickTimerHandle);
	BattleTarget->GetWorld()->GetTimerManager().ClearTimer(DurationTimerHandler);
	
	BattleTarget->RemoveStatusEffect(this); //This should be the last line always, as it will delete THIS!
}

bool FStatusEffect::IsStatusEffect(const UStatusEffectData* AStatusEffectData,const UBattleInfoComponent* TheBattleCreator) const
{
	return AStatusEffectData == StatusEffectData && BattleCreator == TheBattleCreator;
}

bool FStatusEffect::IsStatusEffect(const UStatusEffectData* AStatusEffectData) const
{
	return AStatusEffectData == StatusEffectData;
}

bool FStatusEffect::IsRemovable() const
{
	return StatusEffectData->IsRemovable();
}

int32 FStatusEffect::ObtainEffectResourceTypeValue(UResourceTypeData* AStatusEffectData) const
{
	if(UEffect* Effect = StatusEffectData->GetEffect().GetDefaultObject())
	{
		int32 AmountAffected{0};
		Effect->ObtainAffectedResourceValue(AStatusEffectData,AmountAffected);
		return AmountAffected;
	}
	return 0;
}

int32 FStatusEffect::ObtainAffectedValue(EAffectedValue& AffectedValue) const
{
	if(UEffect* Effect = StatusEffectData->GetEffect().GetDefaultObject())
	{
		int32 AmountAffected{0};
		Effect->ObtainAffectedValue(AffectedValue,AmountAffected);
		return AmountAffected;
	}
	return 0;
}

