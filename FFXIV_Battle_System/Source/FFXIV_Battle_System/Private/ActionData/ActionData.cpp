// Fill out your copyright notice in the Description page of Project Settings.


#include "ActionData/ActionData.h"

FName UActionData::GetActionName() const
{
	return ActionName;
}

UCastData* UActionData::GetCastData() const
{
	return Cast;
}

UCostData* UActionData::GetCostData() const
{
	return Cost;
}

UCooldownData* UActionData::GetCooldownData() const
{
	return Cooldown;
}

TArray<ETargetType> UActionData::GetTargetPriorityList() const
{
	return TargetPriorityList;
}

TSubclassOf<UEffect> UActionData::GetEffects() const
{
	return Effect;
}
