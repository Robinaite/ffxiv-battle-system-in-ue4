// Fill out your copyright notice in the Description page of Project Settings.


#include "ActionData/CostData.h"

UResourceTypeData* UCostData::GetResourceType() const
{
	return ResourceType;
}

int32 UCostData::GetCostAmount() const
{
	return Amount;
}
