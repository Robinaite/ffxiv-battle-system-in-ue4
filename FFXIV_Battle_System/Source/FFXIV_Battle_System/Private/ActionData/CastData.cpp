// Fill out your copyright notice in the Description page of Project Settings.


#include "ActionData/CastData.h"

bool UCastData::HasCastTime() const
{
	return Time > 0;
}

float UCastData::GetCastTime() const
{
	return Time;
}
