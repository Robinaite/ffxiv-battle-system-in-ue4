// Copyright Epic Games, Inc. All Rights Reserved.

#include "FFXIV_Battle_System.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, FFXIV_Battle_System, "FFXIV_Battle_System" );
 