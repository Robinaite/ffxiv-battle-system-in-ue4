// Copyright Epic Games, Inc. All Rights Reserved.

#include "FFXIV_Battle_SystemGameMode.h"
#include "FFXIV_Battle_SystemCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFFXIV_Battle_SystemGameMode::AFFXIV_Battle_SystemGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
